//
//  ShareToSocialViewController.swift
//  Doodlet
//
//  Created by Han Wang on 11/4/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse
import FBSDKShareKit

class ShareToSocialViewController: UIViewController {
    
    // MARK: - Model
    
    var photo : UIImage!
    
    // MARK: - Actions
    
    @IBAction func shareToFB(sender: UIButton) {
        let photoToShare = FBSDKSharePhoto.init(image: photo, userGenerated: true)
        photoToShare.caption = "Test Image" //can change
        let photoContent = FBSDKSharePhotoContent.init()
        photoContent.photos = [photoToShare]
        FBSDKShareDialog.showFromViewController(self, withContent: photoContent, delegate: nil)
    }
    
    @IBAction func saveTapped() {
        UIImageWriteToSavedPhotosAlbum(photo, self, nil, nil)
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
