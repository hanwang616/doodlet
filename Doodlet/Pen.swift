//
//  Pen.swift
//  Doodlet
//
//  Created by Han Wang on 11/5/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit

class Pen {
    var name: String
    var icon: UIImage?
    var r: CGFloat
    var g: CGFloat
    var b: CGFloat
    var alpha: CGFloat
    var lineCap: CGLineCap
    var lineDash: [CGFloat]?
    var lineJoin: CGLineJoin
    var lineWidth: CGFloat
    var shadowX: CGFloat?
    var shadowY: CGFloat?
    var shadowBlur: CGFloat?
    
    init(name _name: String, icon _icon: UIImage?, r _r: CGFloat, g _g: CGFloat, b _b: CGFloat, alpha _alpha: CGFloat, lineCap _lineCap: CGLineCap, lineDash _lineDash: [CGFloat]?, lineJoin _lineJoin: CGLineJoin, lineWidth _lineWidth: CGFloat, shadowX _shadowX: CGFloat?, shadowY _shadowY: CGFloat?, shadowBlur _shadowBlur: CGFloat?) {
        name = _name
        icon = _icon
        r = _r
        g = _g
        b = _b
        alpha = _alpha
        lineCap = _lineCap
        lineDash = _lineDash
        lineJoin = _lineJoin
        lineWidth = _lineWidth
        shadowX = _shadowX
        shadowY = _shadowY
        shadowBlur = _shadowBlur
    }
    
    func getUIColor() -> UIColor {
        return UIColor.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    func getCGColor() -> CGColor {
        return getUIColor().CGColor
    }
    
    func getCIColor() -> CIColor {
        return getUIColor().CIColor
    }
}
