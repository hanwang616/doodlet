//
//  AddGroupViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/22/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse

class AddGroupViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet var addNewGroupTextLabel: UILabel!
    @IBOutlet var groupNameTextField: UITextField!
    @IBOutlet var groupDescTextField: UITextField!
    
    // MARK: - Actions
    
    @IBAction func addGroup(sender: AnyObject) {
        
        if groupNameTextField.text == "" {
            groupNameTextField.attributedText = NSAttributedString.init(string: "Please Enter Group Number!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        if groupDescTextField.text == "" {
            groupNameTextField.attributedText = NSAttributedString.init(string: "Please Enter Group Description!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        let newGroup = PFObject(className: "Group");
        newGroup["groupName"] = groupNameTextField.text
        newGroup["desc"] = groupDescTextField.text
        newGroup["school"] = PFUser.currentUser()?.objectForKey("school")
        newGroup["whoAdded"] = PFUser.currentUser()
        
        newGroup.saveInBackgroundWithBlock({ (success, error) -> Void in
            if (success && error == nil) {
                let newGroupReg = PFObject(className: "GroupReg");
                newGroupReg["user"] = PFUser.currentUser()
                newGroupReg["group"] = newGroup
                newGroupReg.saveInBackgroundWithBlock({ (success, error) -> Void in
                    if (success && error == nil) {
                        self.performSegueWithIdentifier("newGroupAdded", sender: self)
                    } else {
                        print(error)
                    }
                })
            } else {
                print(error)
            }
        })
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
