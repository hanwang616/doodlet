//
//  SettingsViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/22/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: - Outlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var changePasswordTextLabel: UILabel!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var repasswordTextField: UITextField!
    @IBOutlet var displayNameTextField: UITextField!
    @IBOutlet var mySchoolPickerView: UIPickerView!
    
    // MARK: - Model
    
    var schoolList = [PFObject(className: "School")]
    var mySchool = PFObject(className: "School")
    var mySchoolIndex : Int = 0
    
    // MARK: - Actions
    
    @IBAction func changePassword(sender: AnyObject) {
        if (PFUser.currentUser()!.objectForKey("thirdPartyAuth") as! Bool == true) {
            let alert = UIAlertController.init(title: "Cannot Save Password", message: "Login with your social account!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        if passwordTextField.text?.characters.count < 10 {
            passwordTextField.text = ""
            passwordTextField.attributedPlaceholder = NSAttributedString.init(string: "Minimum 10 Characters!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        if passwordTextField.text != repasswordTextField.text {
            repasswordTextField.text = ""
            repasswordTextField.attributedPlaceholder = NSAttributedString.init(string: "Does Not Match Password!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        PFUser.currentUser()?.password = passwordTextField.text
        PFUser.currentUser()?.saveInBackgroundWithBlock({ (success, error) -> Void in
            PFUser.logInWithUsernameInBackground((PFUser.currentUser()?.username)!, password: self.passwordTextField.text!, block: { (user, error) -> Void in
                if ((user) != nil) {
                    self.changePasswordTextLabel.attributedText = NSAttributedString.init(string: "New Password Saved!", attributes: [NSForegroundColorAttributeName: UIColor.greenColor() ])
                } else {
                    print(error)
                }
            })
        })
    }
    
    // MARK: - Lifecycle
    override func viewWillAppear(animated: Bool) {
        emailTextField.text = PFUser.currentUser()?.username
        displayNameTextField.text = PFUser.currentUser()!.objectForKey("displayName") as? String
        
        //fetch school list for the PickerView
        let schoolQuery: PFQuery = PFQuery(className: "School")
        schoolQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let results = results as [PFObject]! {
                    for result in results {
                        self.schoolList.append(result)
                        if PFUser.currentUser()!.objectForKey("school") != nil && result.objectId == PFUser.currentUser()!.objectForKey("school")!.objectId {
                            self.mySchool = result
                            self.mySchoolIndex = self.schoolList.indexOf(result)!
                        }
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.mySchoolPickerView.reloadAllComponents()
                    self.mySchoolPickerView.selectRow(self.mySchoolIndex, inComponent: 0, animated: false)
                })
            } else {
                print(error)
            }
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if (emailTextField.text as String!).containsString("@") && emailTextField.text?.characters.count < 10 {
            PFUser.currentUser()!.username = emailTextField.text
            PFUser.currentUser()!["email"] = emailTextField.text
        }
        
        PFUser.currentUser()!["displayName"] = displayNameTextField.text
        
        if PFUser.currentUser()!.objectForKey("school") == nil || PFUser.currentUser()!.objectForKey("school") as! PFObject != mySchool {
            
            PFUser.currentUser()!["school"] = mySchool
            
            //remove all GroupReg
            let groupRegQuery: PFQuery = PFQuery(className: "GroupReg")
            groupRegQuery.whereKey("user", equalTo: PFUser.currentUser()!)
            groupRegQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
                if error == nil {
                    if let results = results as [PFObject]! {
                        PFObject.deleteAllInBackground(results)
                    }
                } else {
                    print(error)
                }
            }
        }
        
        PFUser.currentUser()?.saveInBackground()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return schoolList.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "- Select Your School -"
        }
        return schoolList[row].objectForKey("name") as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 { //no school is selected
            return
        }
        mySchool = schoolList[row]
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
