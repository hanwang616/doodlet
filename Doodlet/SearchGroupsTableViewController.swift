//
//  SearchGroupsTableViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/17/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse

class SearchGroupsTableViewController: UITableViewController, UISearchBarDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet var groupSearchBar: UISearchBar!
    @IBOutlet var resultTable: UITableView!
    
    // MARK: - Model
    
    var searchResultGroups = [PFObject(className: "Group")]
    
    // MARK: - Control
    
    var tag: Int = 0 //to differentiate pending queries
    
    // MARK: - Search Bar
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        ++tag
        let myTag = self.tag;
        let until : dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, 500000000) //wait 0.5 sec before fire that query
        dispatch_after(until, dispatch_get_main_queue(), { () -> Void in
            if myTag == self.tag {
                self.searchResultGroups = []
                
                if (searchText == "") {
                    self.resultTable.reloadData()
                    return
                }
                
                let groupNameQuery: PFQuery = PFQuery(className: "Group")
                groupNameQuery.whereKey("school", equalTo: (PFUser.currentUser()?.objectForKey("school"))!)
                groupNameQuery.whereKey("groupName", containsString: searchText)
                
                groupNameQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
                    if error == nil {
                        if let results = results as [PFObject]! {
                            self.searchResultGroups = []
                            for result in results {
                                if (!self.searchResultGroups.contains(result) ) {self.searchResultGroups.append(result)}
                            }
                            
                            let groupDescQuery: PFQuery = PFQuery(className: "Group")
                            groupDescQuery.whereKey("school", equalTo: (PFUser.currentUser()?.objectForKey("school"))!)
                            groupDescQuery.whereKey("desc", containsString: searchText)
                            
                            groupDescQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
                                if error == nil {
                                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                        if let results = results as [PFObject]! {
                                            for result in results {
                                                if (!self.searchResultGroups.contains(result)) {self.searchResultGroups.append(result)}
                                            }
                                        }
                                        self.resultTable.reloadData()
                                    })
                                } else {
                                    print(error)
                                }
                            }
                        }
                    } else {
                        print(error)
                    }
                }
                
            } else {
                return //if other queries dispatched after me, don't fire me
            }
        })
        
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        searchResultGroups = []
        tag = 0
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView.init(frame: CGRectZero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return searchResultGroups.count + 1 //extra row for "Add New Group"
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == searchResultGroups.count { //"Add New Group"
            let cell = tableView.dequeueReusableCellWithIdentifier("addGroup", forIndexPath: indexPath)
                
            cell.textLabel!.text = "Add Another Group"
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("group", forIndexPath: indexPath)
        let group = self.searchResultGroups[indexPath.row]
        cell.textLabel?.text = group.objectForKey("groupName") as? String
        cell.detailTextLabel?.text = group.objectForKey("desc") as? String
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == searchResultGroups.count { //"Add New Group"
            self.performSegueWithIdentifier("addGroup", sender: self)
            return
        }
        
        let group = self.searchResultGroups[indexPath.row]
        
        let groupRegQuery: PFQuery = PFQuery(className: "GroupReg")
        groupRegQuery.whereKey("user", equalTo: PFUser.currentUser()!)
        groupRegQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let results = results as [PFObject]! {
                    for result in results {
                        if (result.objectForKey("group") as! PFObject) == group { //alert and return
                            let alert = UIAlertController.init(title: "Failure", message: "Group already registered!", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                            return
                        }
                    }
                    let newGroupReg = PFObject(className: "GroupReg");
                    newGroupReg["user"] = PFUser.currentUser()
                    newGroupReg["group"] = group
                    newGroupReg.saveInBackgroundWithBlock({ (success, error) -> Void in
                        if (success && error == nil) {
                            self.performSegueWithIdentifier("newGroupRegistered", sender: self)
                        } else {
                            print(error)
                        }
                    })
                }
            } else {
                print(error)
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
