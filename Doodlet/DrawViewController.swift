//
//  ViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/2/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse

class DrawViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet var drawView: UIImageView!
    @IBOutlet var msgTextField: UITextField!
    @IBOutlet var msgSendButton: UIButton!
    @IBOutlet var statsTextLabel: UILabel!
    
    // MARK: - Model
    var lines: [Line] = []
    var pens: [Pen] = []
    
    // MARK: - View
    var lastPoint: CGPoint!
    var currentPen: Pen!
    var touchMoved = false
    
    // MARK: - Control
    var createdAtOfLastFetchedMsg : NSDate!
    var createdAtOfLastFetchedLine : NSDate!
    var fetchFromServerTimer : NSTimer!
    var group = PFObject(className: "Group")
    var viewDidLoadNew = true
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        drawLines()
        
        createdAtOfLastFetchedMsg = NSDate.init()
        fetchFromServer()
        fetchFromServerTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("fetchFromServer"), userInfo: nil, repeats: true)
        
        fetchPenFromServer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        msgTextField.hidden = true
        msgSendButton.hidden = true
        
        viewDidLoadNew = true
        
        createdAtOfLastFetchedLine = NSDate.init(timeIntervalSince1970: 0)
    }
    
    override func viewDidDisappear(animated: Bool) {
        fetchFromServerTimer.invalidate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "share") {
            (segue.destinationViewController as! ShareToSocialViewController).photo = drawView.image
        }
    }
    
    // MARK: - Actions
    
    @IBAction func showMsgTools(sender: AnyObject) {
        msgTextField.hidden = !msgTextField.hidden
        msgSendButton.hidden = !msgSendButton.hidden
    }
    
    @IBAction func sendMsg(sender: AnyObject) {
        if (msgTextField.text == "") {
            view.endEditing(true)
            msgTextField.hidden = true
            msgSendButton.hidden = true
        } else {
            let newMsg = PFObject(className: "Message")
            newMsg["message"] = (PFUser.currentUser()!.objectForKey("displayName")) as! String + ": " + msgTextField.text!
            newMsg["group"] = group
            newMsg.saveInBackgroundWithBlock({ (success, error) -> Void in
                if (success) {
                    self.view.endEditing(true)
                    self.msgTextField.hidden = true
                    self.msgTextField.text = ""
                    self.msgSendButton.hidden = true
                } else {
                    print(error)
                }
                
            })
        }
    }

    func penTapped(sender: UIButton) {
        for pen in pens {
            if pen.name == sender.titleLabel?.text {
                currentPen = pen
                return
            }
        }
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
        
        if (!msgTextField.hidden || !msgSendButton.hidden) {
            msgTextField.hidden = true
            msgSendButton.hidden = true
        }
        
        touchMoved = false
        lastPoint = (touches.first!).locationInView(self.drawView)
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        touchMoved = true
        let newPoint = (touches.first!).locationInView(self.drawView)
        let newLine = Line.init(start: lastPoint, end: newPoint, r: currentPen.r, g: currentPen.g, b: currentPen.b, alpha: currentPen.alpha, lineCap: currentPen.lineCap, lineDash: currentPen.lineDash, lineJoin: currentPen.lineJoin, lineWidth: currentPen.lineWidth, shadowX: currentPen.shadowX, shadowY: currentPen.shadowY, shadowBlur: currentPen.shadowBlur, createdAt: NSDate.init())
        addLine(newLine)
        
        lastPoint = newPoint
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //if !touchMoved draw a dot and set image
        if !touchMoved {
            let newLine = Line.init(start: lastPoint, end: lastPoint, r: currentPen.r, g: currentPen.g, b: currentPen.b, alpha: currentPen.alpha, lineCap: currentPen.lineCap, lineDash: currentPen.lineDash, lineJoin: currentPen.lineJoin, lineWidth: currentPen.lineWidth, shadowX: currentPen.shadowX, shadowY: currentPen.shadowY, shadowBlur: currentPen.shadowBlur, createdAt: NSDate.init())
            addLine(newLine)
        }
    }
    
    // MARK: - Utilities
    
    func fetchFromServer() {
        fetchMsgFromServer()
        fetchDrawingFromServer()
    }
    
    func fetchPenFromServer() {
        let query = PFQuery(className: "Pen")

        query.findObjectsInBackgroundWithBlock { (pens: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let pens = pens as [PFObject]! {
                    for pen in pens {
                        let name = pen.objectForKey("name") as! String
                        //NSFileManager.cre
                        let icon = pen.objectForKey("icon") as? UIImage
                        let r = pen.objectForKey("r") as! CGFloat
                        let g = pen.objectForKey("g") as! CGFloat
                        let b = pen.objectForKey("b") as! CGFloat
                        let alpha = pen.objectForKey("alpha") as! CGFloat
                        let lineCap = CGLineCap.init(rawValue: Int32.init(pen.objectForKey("lineCap") as! Int))!
                        let lineDash = pen.objectForKey("lineDash") as? [CGFloat]
                        let lineJoin = CGLineJoin.init(rawValue: Int32.init(pen.objectForKey("lineJoin") as! Int))!
                        let lineWidth = pen.objectForKey("lineWidth") as! CGFloat
                        let shadowX = pen.objectForKey("shadowX") as? CGFloat
                        let shadowY = pen.objectForKey("shadowY") as? CGFloat
                        let shadowBlur = pen.objectForKey("shadowBlur") as? CGFloat
                        let newPen = Pen.init(name: name, icon: icon, r: r, g: g, b: b, alpha: alpha, lineCap: lineCap, lineDash: lineDash, lineJoin: lineJoin, lineWidth: lineWidth, shadowX: shadowX, shadowY: shadowY, shadowBlur: shadowBlur)
                        self.pens.append(newPen)
                    }
                    self.currentPen = self.pens.first
                    
                    //create pen buttons
                    for var i = 0; i < self.pens.count; ++i {
                        let penBtn = UIButton.init(frame: CGRect.init(x: CGFloat.init(i * 60), y: self.view.frame.size.height - 60, width: 60, height: 60))
                        
                        
                        //self.view.addConstraint(NSLayoutConstraint.init(item: penBtn, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.drawView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
                        //self.view.addConstraint(NSLayoutConstraint.init(item: penBtn, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: i==0 ? self.drawView : self.pens[i-1], attribute: i==0 ? NSLayoutAttribute.Right : NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
                        penBtn.setTitle(self.pens[i].name, forState: UIControlState.Normal)
                        penBtn.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
                        //penBtn.setImage(self.pens[i].icon, forState: UIControlState.Normal)
                        print(self.pens[i].icon)
                        penBtn.addTarget(self, action: "penTapped:", forControlEvents: UIControlEvents.TouchUpInside)
                        self.view.addSubview(penBtn)
                    }
                }
            } else {
                print(error)
            }
        }
    }
    
    func fetchMsgFromServer() {
        let query = PFQuery(className: "Message")
        query.whereKey("group", equalTo: group)
        query.whereKey("createdAt", greaterThan: createdAtOfLastFetchedMsg)
        
        query.findObjectsInBackgroundWithBlock { (msgs: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let msgs = msgs as [PFObject]! {
                    var msgStrings = [String]()
                    for msg in msgs {
                        let createdAt = msg.createdAt
                        if self.createdAtOfLastFetchedMsg.laterDate(createdAt!).isEqualToDate(createdAt!) {
                            self.createdAtOfLastFetchedMsg = createdAt
                        }
                        msgStrings.append(msg.objectForKey("message") as! String)
                    }
                    self.displayMsg(msgStrings)
                }
            } else {
                print(error)
            }
        }
    }
    
    func fetchDrawingFromServer() {
        let query = PFQuery(className: "Line")
        query.whereKey("createdAt", greaterThan: createdAtOfLastFetchedLine)
        if (PFUser.currentUser() != nil && !viewDidLoadNew) {
            query.whereKey("user", notEqualTo: PFUser.currentUser()!)
        }
        query.whereKey("group", equalTo: group)
        query.limit = 1000
        
        query.findObjectsInBackgroundWithBlock { (lines: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let lines = lines as [PFObject]! {
                    for line in lines {
                        let start_x = line["start_x"]
                        let start_y = line["start_y"]
                        let end_x = line["end_x"]
                        let end_y = line["end_y"]
                        let r = line["r"] as! CGFloat
                        let g = line["g"] as! CGFloat
                        let b = line["b"] as! CGFloat
                        let alpha = line["alpha"] as! CGFloat
                        let lineCap = CGLineCap.init(rawValue: Int32.init(line["lineCap"] as! Int))!
                        let lineDash = line["lineDash"] as? [CGFloat]
                        let lineJoin = CGLineJoin.init(rawValue: Int32.init(line["lineJoin"] as! Int))!
                        let lineWidth = line["lineWidth"] as! CGFloat
                        let shadowX = line["shadowX"] as? CGFloat
                        let shadowY = line["shadowY"] as? CGFloat
                        let shadowBlur = line["shadowBlur"] as? CGFloat
                        
                        let createdAt = line.createdAt
                        
                        if self.createdAtOfLastFetchedLine.laterDate(createdAt!).isEqualToDate(createdAt!) {
                            self.createdAtOfLastFetchedLine = createdAt
                        }
                        
                        let newFetchedLine = Line.init(start: CGPointMake(start_x as! CGFloat, start_y as! CGFloat), end: CGPointMake(end_x as! CGFloat, end_y as! CGFloat), r: r, g: g, b: b, alpha: alpha, lineCap: lineCap, lineDash: lineDash, lineJoin: lineJoin, lineWidth: lineWidth, shadowX: shadowX, shadowY: shadowY, shadowBlur: shadowBlur, createdAt: createdAt!)
                        
                        //save to local
                        self.lines.append(newFetchedLine)
                    }
                }
            } else {
                print(error)
            }
            self.drawLines()
            self.viewDidLoadNew = false
        }
    }
    
    func displayMsg(msgs: [String]) {
        var delay = 0.0
        for s in msgs {
            let msgLabel = UILabel.init(frame: CGRect.init(x: UIScreen.mainScreen().bounds.size.width, y: drawView.center.y, width: 400, height: 50))
            msgLabel.text = s
            view.addSubview(msgLabel)
            
            UIView.animateWithDuration(5, delay: delay, options: UIViewAnimationOptions.CurveLinear,
                
                animations: { () -> Void in
                msgLabel.center = CGPointMake(0, self.drawView.center.y)
                },
                
                completion: { (success) -> Void in
                    msgLabel.removeFromSuperview()
            })
            
            delay = delay + 1
        }
    }
    
    func addLine(newLine: Line) {
        //when lines reached max number can be fetched
        if (lines.count >= 1000) {
            return
        }
        
        lines.append(newLine)
        
        drawLines()
        
        //save to server
        let newPFLine = PFObject(className: "Line")
        newPFLine["user"] = PFUser.currentUser()
        newPFLine["start_x"] = newLine.start.x
        newPFLine["start_y"] = newLine.start.y
        newPFLine["end_x"] = newLine.end.x
        newPFLine["end_y"] = newLine.end.y
        newPFLine["r"] = newLine.r
        newPFLine["g"] = newLine.g
        newPFLine["b"] = newLine.b
        newPFLine["alpha"] = newLine.alpha
        newPFLine["lineCap"] = Int.init(newLine.lineCap.rawValue)
        if (newLine.lineDash != nil) {
            newPFLine["lineDash"] = NSArray.init(array: newLine.lineDash!)
        }
        newPFLine["lineJoin"] = Int.init(newLine.lineJoin.rawValue)
        newPFLine["lineWidth"] = newLine.lineWidth
        if (newLine.shadowX != nil && newLine.shadowY != nil && newLine.shadowBlur != nil) {
            newPFLine["shadowX"] = newLine.shadowX
            newPFLine["shadowY"] = newLine.shadowY
            newPFLine["shadowBlur"] = newLine.shadowBlur
        }
        
        newPFLine["group"] = group
        newPFLine.saveInBackground()
    }
    
    func drawLines() {
        //update stats
        statsTextLabel.text = (Float.init(lines.count) / Float.init(1000)).description
        
        //sort the lines to be drawn oldest first
        var sortedLines = [Line]()
        for line in lines {
            if (!line.isDrawn) {
                sortedLines.append(line)
                line.isDrawn = true
            }
        }
        sortedLines = sortedLines.sort({ (object1, object2) -> Bool in
            return object1.createdAt.laterDate(object2.createdAt).isEqualToDate(object1.createdAt)
        })
        
        //draw
        UIGraphicsBeginImageContext(self.drawView.frame.size)
        let context = UIGraphicsGetCurrentContext()
        self.drawView.image?.drawInRect(CGRectMake(0, 0, self.drawView.frame.size.width, self.drawView.frame.size.height))
        CGContextSetBlendMode(context, CGBlendMode.Normal)
        for line in sortedLines {
            CGContextMoveToPoint(context, line.start.x, line.start.y)
            CGContextAddLineToPoint(context, line.end.x, line.end.y)
            
            //line-specific settings
            CGContextSetLineCap(context, line.lineCap)
            if (line.lineDash != nil) {
                CGContextSetLineDash(context, 0, line.lineDash!, line.lineDash!.count)
            } else {
                CGContextSetLineDash(context, 0, nil, 0)
            }
            CGContextSetLineJoin(context, line.lineJoin)
            CGContextSetLineWidth(context, line.lineWidth)
            if (line.shadowX != nil && line.shadowY != nil && line.shadowBlur != nil) {
                CGContextSetShadowWithColor(context, CGSize.init(width: line.shadowX!, height: line.shadowY!), line.shadowBlur!, line.getCGColor())
            } else {
                CGContextSetShadowWithColor(context, CGSize.zero, CGFloat.init(), nil)
            }
            CGContextSetStrokeColorWithColor(context, line.getCGColor())
            
            CGContextStrokePath(context)
        }
        self.drawView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }

}

