//
//  Line.swift
//  Doodlet
//
//  Created by Han Wang on 9/18/15.
//  Copyright (c) 2015 Han Wang. All rights reserved.
//

import UIKit


class Line {
    
    var start: CGPoint
    var end: CGPoint
    var r: CGFloat
    var g: CGFloat
    var b: CGFloat
    var alpha: CGFloat
    var lineCap: CGLineCap
    var lineDash: [CGFloat]?
    var lineJoin: CGLineJoin
    var lineWidth: CGFloat
    var shadowX: CGFloat?
    var shadowY: CGFloat?
    var shadowBlur: CGFloat?
    
    var createdAt: NSDate
    var isDrawn: Bool
    
    init(start _start: CGPoint, end _end: CGPoint, r _r: CGFloat, g _g: CGFloat, b _b: CGFloat, alpha _alpha: CGFloat, lineCap _lineCap: CGLineCap, lineDash _lineDash: [CGFloat]?, lineJoin _lineJoin: CGLineJoin, lineWidth _lineWidth: CGFloat, shadowX _shadowX: CGFloat?, shadowY _shadowY: CGFloat?, shadowBlur _shadowBlur: CGFloat?, createdAt _createdAt: NSDate) {
        start =  _start
        end = _end
        r = _r
        g = _g
        b = _b
        alpha = _alpha
        lineCap = _lineCap
        lineDash = _lineDash
        lineJoin = _lineJoin
        lineWidth = _lineWidth
        shadowX = _shadowX
        shadowY = _shadowY
        shadowBlur = _shadowBlur
        createdAt = _createdAt
        isDrawn = false
    }

    func getUIColor() -> UIColor {
        return UIColor.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    func getCGColor() -> CGColor {
        return getUIColor().CGColor
    }
    
    func getCIColor() -> CIColor {
        return getUIColor().CIColor
    }
    
}