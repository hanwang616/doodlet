//
//  LoginViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/16/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4
import ParseTwitterUtils

import Fabric
import TwitterKit

class LoginViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    // MARK: - Actions
    
    @IBAction func login(sender: AnyObject) {
        PFUser.logInWithUsernameInBackground(emailTextField.text!, password: passwordTextField.text!, block: { (user, error) -> Void in
            if ((user) != nil) {
                self.performSegueWithIdentifier("loginSuccess", sender: self)
            } else {
                let alert = UIAlertController.init(title: "Login Failed", message: "Invalid Email/Password!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func fbLogin(sender: AnyObject) {
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["public_profile","email","user_education_history"]) { (user, error) -> Void in
            if (user != nil) {
                user!["thirdPartyAuth"] = true;
                //fetch newest user data from Facebook
                FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"email,name,picture"]).startWithCompletionHandler({ (FBSDKGraphRequestConnection, result, error) -> Void in
                    if (result["email"] != nil) {
                        user!.username = result["email"] as? String
                    }
                    user!["displayName"] = result["name"] as! String
                    user!["email"] = result["email"] as! String
                    //to use profile photo: result["picture"]
                    user!.saveInBackground()
                    
                    //extra settings if first time login with Facebook
                    if user?.objectForKey("school") == nil {
                        self.performSegueWithIdentifier("signUpFillProfile", sender: self)
                    } else {
                        self.performSegueWithIdentifier("loginSuccess", sender: self)
                    }
                })
            } else {
                print(error)
            }
        }
    }
    
    @IBAction func twitterLogin(sender: AnyObject) {
        PFTwitterUtils.logInWithBlock { (user, error) -> Void in
            if user != nil {
                user!["thirdPartyAuth"] = true;
                //fetch newest user data from Twitter
                
                
                
                //extra settings if first time login with Twitter
                if user!.objectForKey("school") == nil {
                    self.performSegueWithIdentifier("signUpFillProfile", sender: self)
                } else {
                    self.performSegueWithIdentifier("loginSuccess", sender: self)
                }
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if (PFUser.currentUser() != nil) {
            self.performSegueWithIdentifier("loginSuccess", sender: self)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
