//
//  SocialSignUpSupplementViewController.swift
//  Doodlet
//
//  Created by Han Wang on 10/30/15.
//  Copyright © 2015 Han Wang. All rights reserved.
//

import UIKit
import Parse

class SocialSignUpSupplementViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var displayNameTextField: UITextField!
    @IBOutlet var schoolPickerView: UIPickerView!
    @IBOutlet var schoolLabel: UILabel!
    
    // MARK: - Model
    
    var schoolList = [PFObject(className: "School")]
    var mySchool = PFObject(className: "School")
    var mySchoolIndex : Int = 0
    
    // MARK: - Actions
    
    @IBAction func saveTapped(sender: AnyObject) {
        
        if !(emailTextField.text as String!).containsString("@") {
            emailTextField.text = ""
            emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Please Enter A Valid Email!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        if emailTextField.text?.characters.count < 10 {
            emailTextField.text = ""
            emailTextField.attributedPlaceholder = NSAttributedString.init(string: "Minimum 10 Characters!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        if mySchoolIndex == 0 {
            schoolLabel.attributedText = NSAttributedString.init(string: "Please Select Your School!", attributes: [NSForegroundColorAttributeName: UIColor.redColor() ])
            return
        }
        
        PFUser.currentUser()!.username = emailTextField.text
        PFUser.currentUser()!.email = emailTextField.text
        PFUser.currentUser()!["displayName"] = displayNameTextField.text
        PFUser.currentUser()!["school"] = mySchool
        
        PFUser.currentUser()?.saveInBackgroundWithBlock({ (success, error) -> Void in
            if (success) {
                self.performSegueWithIdentifier("socialSignUpSupplementFinished", sender: self)
            } else {
                print(error)
            }
        })
    }
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        emailTextField.text = PFUser.currentUser()?.email
        displayNameTextField.text = PFUser.currentUser()?.objectForKey("displayName") as? String
        
        let schoolQuery: PFQuery = PFQuery(className: "School")
        
        schoolQuery.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                if let results = results as [PFObject]! {
                    for result in results {
                        self.schoolList.append(result)
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.schoolPickerView.reloadAllComponents()
                })
            } else {
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - PickerView
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return schoolList.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "- Select Your School -"
        }
        return schoolList[row].objectForKey("name") as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        mySchool = schoolList[row]
        mySchoolIndex = row
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
